const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');

module.exports = class BlacklistCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'blacklist',
			group: 'general',
			memberName: 'blacklist',
			description: 'Block a user from bot functions',
			args: [
				{
					key: 'target',
					type: 'member',
					prompt: '',
				}
			],
			argsPromptLimit: 0,
			guildOnly: true,
		});
	}
	hasPermission(msg) {
		return this.client.isOwner(msg.author);
	}
}