const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');

module.exports = class HelpCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'help',
			group: 'general',
			memberName: 'help',
			description: 'Displays this help message.'
		});
	}

	run(msg) {
		const embed = new Discord.MessageEmbed()
			.setTitle(`Polus Bot (Crewlink LFG) Help`)
			.setDescription(`:microphone2: - indicates a command may only be ran in a voice channel \nSome commands have arguments: **<required> [optional]**`)
			.addField(`.invite <map> [crewlink server]`, `:small_blue_diamond::microphone2: Sends an invite to join your game.\n:small_orange_diamond: **<map> |** \`skeld\`, \`mira\`, or \`polus\`\n:small_orange_diamond: **[crewlink server] |** Voice server in Crewlink settings. \n:small_orange_diamond: (default \`${config.voiceServer}\`)`)
			.addField(`Aliases for .invite`, `:small_blue_diamond: You can also use **\`.skeld [crewlink server]\`**, **\`.mira [crewlink server]\`** or **\`.polus [crewlink server]\`**, which does the same thing as \`.invite\``)
			.addField(`.ping`, `:small_blue_diamond::microphone2: Pings everyone in your lobby.\n:small_orange_diamond: Useful if you need to fix Crewlink and people are deafened!`)
			.addField(`.votekick <target> <reason>`, `:small_blue_diamond::microphone2: Call a votekick on a user in your lobby.\n:small_orange_diamond: **<target> |** The user to kick\n:small_orange_diamond: **<reason> |** The reason for calling this votekick`)
			.addField(`.lobbies`, `:small_blue_diamond: Returns a list of active lobbies with an open slot.\n:small_orange_diamond: No need to ask "any spots open?" anymore - check yourself with this command!`)
			.addField(`.mylobby`, `:small_blue_diamond::microphone2: Returns a list of all members in your lobby and their IDs.\n:small_orange_diamond: This is useful if you cannot @mention a user for a votekick (e.g. they have a name with weird characters)`)
			.addField(`.help`, `:small_blue_diamond: This!`)
			.addField(`.info`, `:small_blue_diamond: Returns information about this bot (uptime, version, etc)`)
			.setFooter(`for ${msg.author.tag} | Bot made by steve.#5225`, msg.author.displayAvatarURL())
			.setColor("GREEN")
			.setThumbnail(msg.client.user.displayAvatarURL())
		msg.reply(embed).catch(console.error);
	}
}