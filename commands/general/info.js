const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');
const os = require('os');
const memory = require('memory');

module.exports = class InfoCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'info',
			group: 'general',
			memberName: 'info',
			description: 'Bot info'
		});
	}

	run(msg) {
		var uptimeDays, uptimeHours, uptimeMinutes, uptimeSeconds;
		uptimeSeconds = Math.floor(msg.client.uptime / 1000);
		uptimeMinutes = Math.floor(uptimeSeconds / 60);
		uptimeSeconds = uptimeSeconds % 60;
		uptimeHours = Math.floor(uptimeMinutes / 60);
		uptimeMinutes = uptimeMinutes % 60;
		uptimeDays = Math.floor(uptimeHours / 24);
		uptimeHours = uptimeHours % 24;

		const embed = new Discord.MessageEmbed()
			.setTitle(`Polus Bot Info`)
			.setDescription(`:small_blue_diamond: For a list of commands, use \`.help\`\n:small_blue_diamond: Need help with invites? Use \`.invite help\``)
			.setColor("#7a7373")
			.addField(`:robot: Version`, `${config.botVersion}`, true)
			.addField(`:sunny: Uptime`, `${uptimeDays}d, ${uptimeHours}h, ${uptimeMinutes}m`, true)
			//.addField(`:desktop: Memory Usage`, `${memory()}`, true)
			.addField(`:satellite_orbital: Ping`, `${Date.now() - msg.createdTimestamp}ms`, true)
			.addField(`:gear: Options (for .invite)`, `:small_orange_diamond: Default Crewlink/voice server **|** \`${config.voiceServer}\`\n:small_orange_diamond: Among Us version **|** \`${config.gameVersion}\`\n:small_orange_diamond: Crewlink version **|** \`${config.crewlinkVersion}\``)
			.addField(`:pencil2: Author`, `Bot made by steve.#5225 for the Among Us Proximity Chat server`)
			.setColor("GREY")
			.setThumbnail(msg.client.user.displayAvatarURL())
		msg.reply(embed).catch(console.error);
	}
}