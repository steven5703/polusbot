const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');

module.exports = class RestartCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'restart',
			group: 'general',
			memberName: 'restart',
			description: 'Restart the bot (must run after updating settings via set command or git pull)',
			guildOnly: true,
		});
	}

	hasPermission(msg) {
		return this.client.isOwner(msg.author);
	}

	run(msg) {
		msg.reply(':wave:');
		var logMsg = `Shutdown requested by ${msg.author.tag}. Destroying client. If bot was started via script, it will restart shortly. Uptime: ${this.client.uptime}`; //log message
		console.log(logMsg);
		this.client.channels.cache.get(config.logChannel).send(`:tools: **Shutting down** \`\`\`${logMsg}\`\`\``);

		setTimeout(destroyClient, 500);
		function destroyClient() {
			msg.client.destroy().catch(console.error);
			console.log(`Client destroyed. Exiting...`);
			process.exit(); // Terminate process. This command is intended to be used with PM2. If it is not hooked up to auto start script, it will simply shutdown
        }

    }
}