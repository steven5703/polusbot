const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');
const fs = require('fs');
const updateJsonFile = require('update-json-file');

module.exports = class SettingsCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'set',
			group: 'general',
			memberName: 'set',
			description: 'Used to update crewlink version, recommended voice server, etc.',
			guildOnly: true,
			args: [
				{
					key: "setting",
					prompt: "What would you like to change?",
					default: "print",
					type: "string"
				},
				{
					key: "value",
					prompt: "What would you like to change this to?",
					default: "print",
					type: "string"
				}
			],
			argsPromptLimit: 0,
		});
	}

	hasPermission(msg) {
		return this.client.isOwner(msg.author);
	}

	run(msg, { setting, value }) {
		var s = setting;
		var v = value;
		var configFile = './config/main.json'
		if (s !== "print" && v == "print") { // checks if a setting was specified without a value
			return msg.reply(`:x: You must specify a value to change \`${s}\` to.`);
		} else if (s == "print") {
			fs.readFile(configFile, 'utf8', function (err, data) {
				if (err) {
					msg.reply(`:x: Could not write to config file. Details: \`${err}\``);
					return console.error(err);
				}
				msg.reply("**/config/main.json:**\n```" + data + "```\n*Other: nickname*");
			});
		} else if (s == "voiceServer") {
			updateJsonFile(configFile, (data) => {
				data.voiceServer = value
				return data
			}).catch(function (err) {
				console.error(err);
				return msg.reply(`:x: Could not write to config file. Details: \`${err}\``);
			});
			msg.reply(`:white_check_mark: Changed \`${s}\` to \`${v}\`.\nYou must restart for this change to take effect. Use \`.restart\`.`);
			logger(s, v);
		} else if (s == "gameVersion") {
			updateJsonFile(configFile, (data) => {
				data.gameVersion = value
				return data
			}).catch(function (err) {
				console.error(err);
				return msg.reply(`:x: Could not write to config file. Details: \`${err}\``);
			});
			msg.reply(`:white_check_mark: Changed \`${s}\` to \`${v}\`.\nYou must restart for this change to take effect. Use \`.restart\`.`);
			logger(s, v);
		} else if (s == "crewlinkVersion") {
			updateJsonFile(configFile, (data) => {
				data.crewlinkVersion = value
				return data
			}).catch(function (err) {
				console.error(err);
				return msg.reply(`:x: Could not write to config file. Details: \`${err}\``);
			});
			msg.reply(`:white_check_mark: Changed \`${s}\` to \`${v}\`.\nYou must restart for this change to take effect. Use \`.restart\`.`);
			logger(s, v);
		} else if (s == "nickname") {
			msg.guild.me.setNickname(v).catch(console.error);
			msg.reply(`:white_check_mark: Set my nickname to \`${v}\`.`);
			logger(`the bots nickname in ${msg.guild.name}`, v)
		} else {
			msg.reply(`:x: Setting \`${s}\` doesn't exist or isn't changeable via this command.`);
		}

		function logger(s, v) {
			var logMsg = `>> ${msg.author.tag} set ${s} to ${v} (${configFile})`;
			console.log(logMsg);
			msg.guild.channels.resolve(config.logChannel).send(`:gear: **Config** \`\`\`${logMsg}\`\`\``);
        }
	}
}