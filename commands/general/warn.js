const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const config = require('../../config/main.json');

module.exports = class WarnCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'warn',
			group: 'general',
			memberName: 'warn',
			description: 'Give a warning to a user',
			guildOnly: true,
			args: [
				{
					key: "member",
					prompt: "",
					type: "member"
				},
				{
					key: "reason",
					prompt: "",
					type: "string"
				}
			],
			argsPromptLimit: 0,
		});
	}

	hasPermission(msg) {
		return msg.member.hasPermission("MANAGE_MESSAGES");
	}

	async run(msg, { member, reason }) {
		const embed = new Discord.MessageEmbed()
			.setTitle(`:octagonal_sign: WARNING`)
			.setDescription(`You've been warned in ${msg.guild.name}. Further action may be taken if this behaviour persists. Please make sure you understand our rules before participating!` + "\n```" + reason + "```")
			.setFooter(`Warning issued by ${msg.author.tag}`, msg.author.displayAvatarURL())
			.setTimestamp()
			.setColor("ORANGE")
		await member.send(embed);
		await msg.reply(`:white_check_mark:`);

		const timestamp = new Date().toUTCString();
		const logEmbed = new Discord.MessageEmbed()
			.setTitle(`:octagonal_sign: Warning`)
			.setDescription(`:no_entry: **Target:** ${member.toString()} | \`${member.user.tag}\` | \`${member.id}\` \n:calendar: **Timestamp:** ${timestamp}\n:bust_in_silhouette: **Staff:** ${msg.author.toString()} ` + "\n```" + reason + "```")
			.setFooter(`Warning issued by ${msg.author.tag}`, msg.author.displayAvatarURL())
			.setTimestamp()
			.setColor("ORANGE")

		this.client.channels.cache.get(config.warnChannel).send(logEmbed);
	}
}
