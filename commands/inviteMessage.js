const Discord = require('discord.js');
const config = require('../config/main.json');

module.exports = async function postInvite(msg, mapName, region, voice, lobby) {
	if (mapName == "The Skeld") { // Test if map is valid
		var mapIcon = "https://static.wikia.nocookie.net/among-us-wiki/images/2/29/The_Skeld.png";
	} else if (mapName == "MIRA HQ") {
		var mapIcon = "https://static.wikia.nocookie.net/among-us-wiki/images/e/e5/MIRA_HQ.png";
	} else if (mapName == "Polus") {
		var mapIcon = "https://static.wikia.nocookie.net/among-us-wiki/images/4/4c/Polus.png";
	}

	if (msg.channel.id !== config.inviteChannel) { // Make sure command is ran in #matchmaking
		return msg.reply(`:x: You may only post party invites in ${msg.guild.channels.resolve(config.inviteChannel)}`)
			.then(resp => {
				msg.delete({ timeout: 2000 });
				//resp.delete({ timeout: 8000 });
			}).catch(console.error);
	} else if (!msg.member.voice.channel) { // Check if member is in voice channel
		return msg.reply(`:x: Failed to post lobby invite - you are currently not connected to a voice channel!`)
			.then(resp => {
				msg.delete({ timeout: 2000 });
				//resp.delete({ timeout: 8000 });
			}).catch(console.error);
	}
	if (msg.content.length > 75) { // make sure its not spamming
		return msg.delete();
	}
	if (!isUrl(voice)) { // check if crewlink server is actually a url, because people are stupid
		return msg.reply(`:x: \`${voice}\` seems to be an invalid Crewlink server. A Crewlink server should look like a standard URL, for example, the recommended one is currently: \`${config.voiceServer}\`. You do not have to specify the Crewlink server if you are using the recommended one.`)
	}

	let invite = await msg.member.voice.channel.createInvite({
		maxAge: 86400, // created invite is valid for 15 minutes nvm
		unique: true
	}).catch(console.error);

	const embed = new Discord.MessageEmbed()
		.setAuthor(`Crewlink Matchmaking`, `https://raw.githubusercontent.com/ottomated/CrewLink/master/logo.png`)
		.setTitle(`${mapName} | Need ${10 - lobby.members.size}!`)
		.setDescription(`To post an invite like this, simply run \`.invite <map> [optional crewlink server]\` in this channel. For more information, see \`.invite help\`.\n\n**:green_circle: [Click here to join this party with ${lobby.members.size} other people](${invite.url})**`)
		.addField(`:map: Map`, `${mapName}`, true)
		.addField(`:bellhop: Lobby`, `${lobby.name}`, true)
		.addField(`:inbox_tray: Party Size`, `${lobby.members.size}/${lobby.userLimit}`, true)
		.addField(`:satellite: Crewlink Voice Server`, `\`${voice}\``, false)
		.addField(`Among Us and Crewlink version :white_check_mark:`, `:joystick: Among Us Version: \`${config.gameVersion}\`\n:satellite: Crewlink Version: \`${config.crewlinkVersion}\``, false)
		.setFooter(`from ${msg.author.tag} | ${msg.author.id}`, msg.author.displayAvatarURL())
		.setTimestamp()
		.setThumbnail(mapIcon)
		.setColor("RANDOM")
	msg.channel.send(embed).catch(console.error);
	msg.delete(); // delete message bot was summoned from

	var logMsg = `>> Created lobby invite for ${msg.author.tag}: L:${lobby.name} | U:${lobby.members.size} | M:${mapName} | R:${region} | V:${voice} | I:${invite.url}`;
	console.log(logMsg);

	function isUrl(url) { // verifies that is a url
		var regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
		return regexp.test(url);
	}
}