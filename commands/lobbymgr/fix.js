const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'ping',
			group: 'lobbymgr',
			memberName: 'ping',
			aliases: ['ctrlr', 'fix'],
			description: 'Pings all users connected to your lobby. Useful for getting the attention of deafened users when your Crewlink breaks!',
			guildOnly: true,
			throttling: {
				usages: 1,
				duration: 120,
            }
		});
	}

	run(msg) {
		if (!msg.member.voice.channel) { // check if user is actually in lobby
			return msg.reply(`:x: You must be connected to a lobby to use this command!`)
				.then(resp => {
					msg.delete({ timeout: 2000 });
					//resp.delete({ timeout: 8000 });
				}).catch(console.error);
		}
		if (msg.channel.id !== config.fixChannel) { // check if command is in correct channel
			return msg.reply(`:x: You may only run this command in ${msg.guild.channels.resolve(config.fixChannel)}`)
				.then(resp => {
					msg.delete({ timeout: 2000 });
					//resp.delete({ timeout: 8000 });
				}).catch(console.error);
        }
		let lobby = msg.member.voice.channel
		msg.channel.send(`||${lobby.members.map(m => m.toString()).join(' ')}||\n:loudspeaker: **${lobby.name}** - ${msg.member.toString()} has pinged the lobby! They are probably having difficulties hearing you in game.\n Please have everyone open Crewlink and do **Ctrl+R** and/or undeafen on Discord to help them.`);
		msg.delete().catch(console.error);

		var logMsg = `>> Pinging ${lobby.name}'s members, called by ${msg.author.tag}: ${lobby.members.map(m => m.toString()).join(' ')} `;
		msg.guild.channels.resolve(config.logChannel).send(`:loudspeaker: **Lobby Ping** \`\`\`${logMsg}\`\`\``);
		console.log(logMsg);
	}
}