const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const config = require('../../config/main.json');
const sendInvite = require('../inviteMessage.js')

module.exports = class InviteCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'invite',
			aliases: ['inv', 'i'],
			group: 'lobbymgr',
			memberName: 'invite',
			description: 'Post a lobby invite for your party.',
			args: [
				{
					key: 'map',
					type: 'string',
					prompt: '',
				}, {
					key: 'voiceserver',
					type: 'string',
					default: config.voiceServer,
					prompt: '',
				}
			],
			argsPromptLimit: 0,
			guildOnly: true,
		});
	}

	run(msg, { map, voiceserver }) {
		if (map == "help") { // send help and stop executition
			return sendHelp();
		}

		map = map.toLowerCase(); // make map input case insensitive
		if (map == "s" || map == "skeld") { // Test if map is valid, or help command.
			var mapName = "The Skeld";
		} else if (map == "m" || map == "mira" || map == "mirahq") {
			var mapName = "MIRA HQ";
		} else if (map == "p" || map == "polus") {
			var mapName = "Polus";
		} else {
			return msg.reply(`:question: You did not specify a valid map! Valid options:\n:small_orange_diamond: **The Skeld |** \`s\`, \`skeld\`\n:small_orange_diamond: **MIRA HQ |** \`m\`, \`mira\`, \`mirahq\`\n:small_orange_diamond: **Polus         |** \`p\`, \`polus\``)
			.then(resp => {
				msg.delete({ timeout: 2000 });
				//resp.delete({ timeout: 8000 });
			}).catch(console.error);
		}

		// region checker will go here
		sendInvite(msg, mapName, "Any (NA/EU/ASIA)", voiceserver, msg.member.voice.channel); // region placeholder

		function sendHelp() {
			const embed = new Discord.MessageEmbed()
				.setAuthor(`Crewlink Matchmaking Help`, `https://raw.githubusercontent.com/ottomated/CrewLink/master/logo.png`)
				.setTitle(`How to post a Lobby Invite`)
				.setDescription(`To post an invite, simply run \`.invite <map>\` or \`.invite <map> <crewlink server>\` (without the brackets) in ${msg.guild.channels.resolve(config.inviteChannel)}. A map is required. If you do not specify a voice server, the default one which is currently \`${config.voiceServer}\` will be used. When using an alternative voice server, make sure you supply it in your invite so players know to switch! See ${msg.guild.channels.resolve(config.infoChannel)} for more help with invites and the most up to date versions and working voice servers.`)
				.addField(`Current versions and recommended Crewlink voice server :gear:`, `:joystick: Among Us Version: \`${config.gameVersion}\`\n:satellite: Crewlink Version: \`${config.crewlinkVersion}\`\n:globe_with_meridians: Crewlink Voice Server: \`${config.voiceServer}\`\n\n:loudspeaker: **Note:** Software versions are updated ASAP after an update. Please understand that this must be changed manually, so immediately after an update the settings listed here may not be accurate. Refer to ${msg.guild.channels.resolve(config.infoChannel)} immediately after an update.\n:information_source: See https://crewlinkstatus.xyz/ for a list of working Crewlink voice servers.`, false)
				.addField(`Examples`, `\`.invite mira\` - posts an invite for Mira using the recommended Crewlink server.\n\`.invite skeld https://crewlink.glitch.me\` - posts an invite for Skeld using glitch.me Crewlink server.`)
				.setFooter(`for ${msg.author.tag} | .help for a list of all commands`, msg.author.avatarURL())
				.setColor("GREEN")
			msg.reply(embed).catch(console.error);
			msg.delete(); // delete message bot was summoned from
        }
	}
}
