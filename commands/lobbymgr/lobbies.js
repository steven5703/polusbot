const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');

module.exports = class ListCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'lobbies',
			group: 'lobbymgr',
			memberName: 'lobbies',
			aliases: ['list', 'active'],
			description: 'Displays a list of active lobbies',
			guildOnly: true
		});
	}

	async run(msg) {
		var channels = msg.guild.channels.cache.filter(c => (c.name.includes("Proximity Chat") || c.name.includes("Content Lobby") || c.name.includes("Sheriff")) && c.type === 'voice' && c.members.size !== 0 && !c.full);
		var fullChannels = msg.guild.channels.cache.filter(c => (c.name.includes("Proximity Chat") || c.name.includes("Content Lobby") || c.name.includes("Sheriff")) && c.type === 'voice' && c.full);
		var emptyChannels = msg.guild.channels.cache.filter(c => (c.name.includes("Proximity Chat") || c.name.includes("Content Lobby") || c.name.includes("Sheriff")) && c.type === 'voice' && c.members.size == 0);

		var cleanList = [];
		if (!channels.first()) {
			cleanList.push(`*There are currently no active Proximity lobbies with an open slot*`);
			sendMsg();
		} else {
			channels.forEach(push);
		}

		var count = 0;
		async function push(lobby) {
			let invite = await lobby.createInvite().catch(console.error)
			cleanList.push(`:small_orange_diamond: Needs **${lobby.userLimit - lobby.members.size}** - **${lobby.name}** (${lobby.members.size}/${lobby.userLimit}) -- :green_circle: **[Join](${invite.url})**`);
			count++;
			if (count === channels.size) {
				sendMsg()
            }
		}

		async function sendMsg() {		
			cleanList.sort();
			const embed = new Discord.MessageEmbed()
				.setTitle(`Active Lobbies`)
				.setDescription(`${cleanList.map(x => x.toString()).join('\n')}`)
				.addField(`Other Channels`, `There are **${emptyChannels.size} empty** lobbies and **${fullChannels.size} full** lobbies.`)
				.setFooter(`for ${msg.author.tag} | ${msg.author.id}`, msg.author.displayAvatarURL())
				.setColor("GREEN")
				.setTimestamp()
			msg.reply(embed).catch(console.error);
        }
		
	}
}