const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');
const os = require('os');
const memory = require('memory');

module.exports = class MyLobbyCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'mylobby',
			group: 'lobbymgr',
			memberName: 'mylobby',
			description: 'Lists all users in your lobby',
			guildOnly: true,
		});
	}

	run(msg) {
		if (!msg.member.voice.channel) {
			return msg.reply(`:x: You are not connected to a voice channel`);
		}

		var memberList = msg.member.voice.channel.members.array();
		var cleanList = [];
		memberList.forEach(push);
		function push(m) {
			cleanList.push(`${m.user.tag} (${m.displayName}) -- ${m.id}`);
		}
		console.log(cleanList);

		const embed = new Discord.MessageEmbed()
			.setTitle(`${msg.member.voice.channel.name} User ID List - ${msg.member.voice.channel.members.size}/${msg.member.voice.channel.userLimit}`)
			.setDescription(`\`\`\`${cleanList.map(x => x.toString()).join('\n')}\`\`\``)
			.setFooter(`for ${msg.author.tag} | ${msg.author.id}`, msg.author.displayAvatarURL())
			.setColor("GREY")
			.setTimestamp()
		msg.reply(embed).catch(console.error);
	}
}