const { Command } = require('discord.js-commando');
const config = require('../../config/main.json');
const sendInvite = require('../inviteMessage.js')

module.exports = class InvitePolusCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'polus',
			group: 'lobbymgr',
			memberName: 'polus',
			description: 'Post a lobby invite for your party.',
			args: [
				{
					key: 'voiceserver',
					type: 'string',
					default: config.voiceServer,
					prompt: '',
				}
			],
			argsPromptLimit: 0,
			guildOnly: true,
		});
	}

	run(msg, { voiceserver }) {
		sendInvite(msg, "Polus", "N/A", voiceserver, msg.member.voice.channel);
	}
}