const { Command } = require('discord.js-commando');
const config = require('../../config/main.json');
const sendInvite = require('../inviteMessage.js')

module.exports = class InviteSkeldCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'skeld',
			group: 'lobbymgr',
			memberName: 'skeld',
			description: 'Post a lobby invite for your party.',
			args: [
				{
					key: 'voiceserver',
					type: 'string',
					default: config.voiceServer,
					prompt: '',
				}
			],
			argsPromptLimit: 0,
			guildOnly: true,
		});
	}

	run(msg, { voiceserver }) {
		sendInvite(msg, "The Skeld", "N/A", voiceserver, msg.member.voice.channel);
	}
}