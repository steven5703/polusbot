﻿const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const path = require('path');
const config = require('../../config/main.json');
const { verify } = require('crypto');
const np = require('node-persist');

module.exports = class VotekickCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'votekick',
			aliases: ['kick', 'vk'],
			group: 'lobbymgr',
			memberName: 'votekick',
			description: 'Initiate a votekick against a user in your lobby.',
			args: [
				{
					key: 'target',
					type: 'member',
					prompt: '',
				}, {
					key: 'reason',
					type: 'string',
					prompt: '',
				}
			],
			argsPromptLimit: 0,
			guildOnly: true,
		});
	}

	async run(msg, { target, reason }) {


		// Check if conditions make sense
		if (msg.channel.id !== config.votekickChannel) {
			return msg.reply(`:x: You may only call votekicks in ${msg.guild.channels.resolve(config.votekickChannel)}`);
		}
		if (!msg.member.voice.channel) {
			return msg.reply(`:x: Cannot call a votekick - you are currently not connected to a lobby.`);
		}
		if (msg.member.voice.channel !== target.voice.channel || !target.voice.channel) {
			return msg.reply(`:x: Cannot call a votekick on \`${target.user.tag}\` - they are not in your lobby! Make sure you are tagging the correct user.\nIf you are having difficulty tagging the correct user, it may be easier to votekick by User ID. Use \`.mylobby\` to get the User IDs of those in your lobby.`);
		}
		if (msg.member == target) { // not calling votekick on self
			return msg.reply(`:thinking: Why?`);
        }

		// Declare Arrays
		var pendingList = msg.member.voice.channel.members.array();
		var userIds = [];

		pendingList.forEach(userIdAr);
		function userIdAr(user) {
			userIds.push(user.id);
		}

		var yesList = [];
		var noList = [];
		var votekickId;

		startVotekick(target, msg.member.voice.channel, msg.member, reason, msg.member.voice.channel.members, Math.ceil(msg.member.voice.channel.members.size / 2));
		
		async function startVotekick(target, lobby, host, reason, members, votesReq) {
			const embed = new Discord.MessageEmbed()
				.setAuthor(`A votekick has begun in ${lobby.name}!`, target.user.avatarURL())
				.setTitle(`${target.user.tag}} ${target.nickname ? `${target.nickname}` : ``}`)
				.setDescription(`:infomation_source: **Reason:** ${reason} | **Lobby Size:** ${lobby.members.size}`)
				.addField(`:boot: Yes`, `...`, true)
				.addField(`:clock10: Pending`, `...`, true)
				.addField(`:no_entry_sign: No`, `...`, true)
				.addField(`Lobby members, react with :boot: or :no_entry_sign: to cast your vote.`, `:scales: **${votesReq} votes** required to pass votekick\n:clock10: Votekick will close after **2 minutes**\n:warning: Only members of **${lobby.name}** may interact with this votekick.`, false)
				.setFooter(`Votekick initiated by ${host.user.tag} | ${host.user.id}`, host.user.avatarURL())
				.setColor("#00fdff")
				.setTimestamp();
			msg.channel.send(`||${members.map(m => m.toString()).join(' ')}||\nThis function is currently being tested. If you need more help removing someone from the lobby, contact a staff member."`, embed)
				.then(async function (votekickMsg) {
					votekickMsg.react("👢");
					votekickMsg.react("🚫");
					await updateVotekick(members, host, "👢", votekickMsg, votesReq, lobby, target, host); // automatically have host vote yes
					await updateVotekick(members, target, "🚫", votekickMsg, votesReq, lobby, target, host); // automatically have target vote no

					votekickId = votekickMsg;

					msg.client.on('messageReactionAdd', (reaction, user) => {
						if (user == msg.client.user) {
							return;
                        }
						if (reaction.message == votekickMsg && (reaction.emoji.name == '👢' || reaction.emoji.name == '🚫')) {
							if (userIds.includes(user.id)) {
								console.log(`Is in member array`);
								reaction.users.remove(user);
								return updateVotekick(members, user, reaction.emoji.name, votekickMsg, votesReq, lobby, target, host, expireVotekick);
							} else {
								console.log(`Not in member array`);
								reaction.users.remove(user);
								user.send(`:x: You've already voted or you are not in this channel (or you joined after the votekick was called) *(${lobby.name})*`).catch(console.error);
                            }
						}
					});
				}).catch(console.error);

			var logMsg = `>> Votekick created in ${lobby.name} | Target: ${target.user.tag} | Reason: ${reason} | Host: ${host.user.tag} | VotesReq: ${votesReq}`;
			msg.guild.channels.resolve(config.logChannel).send(`:boot: **Votekick** \`\`\`${logMsg}\`\`\``);
			console.log(logMsg);

			var expireVotekick = await setTimeout(closeVotekick, 1000 * 5 * 60, "expire", target, msg.member, msg.member.voice.channel.members, msg.member.voice.channel, expireVotekick);
		}

		async function updateVotekick(members, member, vote, votekickMsg, votesReq, lobby, target, host, expireVotekick) {

			if (vote == '👢') { // YES
				userIds.splice(userIds.indexOf(member.id), 1); // work around
				pendingList.splice(pendingList.indexOf(member), 1);
				yesList.push(member);
			} else if (vote == '🚫') { // NO
				userIds.splice(userIds.indexOf(member.id), 1);
				pendingList.splice(pendingList.indexOf(member), 1);
				noList.push(member);
			} else {
				msg.channel.send("Invalid reaction?!");
			}
			console.log(userIds);

			// Update votekick embed with new
			const newEmbed = new Discord.MessageEmbed()
				.setAuthor(`A votekick has begun in ${lobby.name}!`, target.user.displayAvatarURL())
				.setTitle(`${target.user.tag} ${target.nickname ? `(${target.nickname})` : ``}`)
				.setDescription(`:information_source: **Reason:** ${reason} | **Lobby Size:** ${lobby.members.size}`)
				.addField(`:boot: Yes (${yesList.length})`, `${yesList.map(m => m.toString()).join('\n')}.`, true)
				.addField(`:clock10: Pending (${pendingList.length})`, `${pendingList.map(m => m.toString()).join('\n')}.`, true)
				.addField(`:no_entry_sign: No (${noList.length})`, `${noList.map(m => m.toString()).join('\n')}.`, true)
				.addField(`Lobby members, react with :boot: or :no_entry_sign: to cast your vote.`, `:scales: **${votesReq} votes** required to pass votekick\n:clock10: Votekick will close after **5 minutes**\n:warning: Only members of **${lobby.name}** may interact with this votekick.`, false)
				.setFooter(`Votekick initiated by ${host.user.tag} | ${host.user.id}`, host.user.displayAvatarURL())
				.setTimestamp()
				.setColor("#00fdff")
			votekickMsg.edit(`||${ members.map(m => m.toString()).join(' ') }||\nThis function is currently being tested. If you need more help removing someone from the lobby, contact a staff member.`,newEmbed).catch(console.error);

			// Decide to collect more votes, pass, or fail
			if (yesList.length >= votesReq) {
				return closeVotekick("pass", target, host, members, lobby, votekickMsg, expireVotekick);
			} else if (noList.length > msg.member.voice.channel.members.size - votesReq) {
				return closeVotekick("fail", target, host, members, lobby, votekickMsg, expireVotekick);
			}
        }

		function closeVotekick(status, target, host, members, lobby, votekickMsg, expireVotekick) {
			clearTimeout(expireVotekick);
			votekickId.reactions.removeAll();

			var logMsg = `>> Votekick closed in ${lobby.name} | Status: ${status} | Target: ${target.user.tag} | Reason: ${reason} | Host: ${host.user.tag}`;
			msg.guild.channels.resolve(config.logChannel).send(`:boot: **Votekick** \`\`\`${logMsg}\`\`\``);
			console.log(logMsg);


			if (status == "fail") {
				// Votekick fails
				const embed = new Discord.MessageEmbed()
					.setAuthor(`Votekick closed in ${lobby.name}`, target.user.displayAvatarURL())
					.setTitle(`:x: Votekick failed (${target.displayName})`)
					.setDescription(`:information_source: **Reason:** ${reason} | **Host:** ${host.toString()} | **Target:** ${target.toString()}\n**:clipboard: Results:** ${yesList.length} yes, ${noList.length} no (${pendingList.length} abstain)`)
					.setFooter(`Votekick initiated by ${host.user.tag} | ${host.user.id}`, host.user.displayAvatarURL())
					.setTimestamp()
					.setColor("#ff0000");
				msg.channel.send(`||${members.map(m => m.toString()).join(' ')}||`, embed)
			} else if (status == "pass") {
				// Votekick passes
				const embed = new Discord.MessageEmbed()
					.setAuthor(`Votekick closed in ${lobby.name}`, target.user.displayAvatarURL())
					.setTitle(`:white_check_mark: Kicking ${target.displayName}`)
					.setDescription(`:information_source: **Reason:** ${reason} | **Host:** ${host.toString()} | **Target:** ${target.toString()}\n**:clipboard: Results:** ${yesList.length} yes, ${noList.length} no (${pendingList.length} abstain)`)
					.setFooter(`Votekick initiated by ${host.user.tag} | ${host.user.id}`, host.user.displayAvatarURL())
					.setTimestamp()
					.setColor("#43ff39");
				msg.channel.send(`||${members.map(m => m.toString()).join(' ')}||`, embed);
				applyVotekick(target, lobby);
			} else if (status = "expire") {
				// votekick expires after 2 minutes
				const embed = new Discord.MessageEmbed()
					.setAuthor(`Votekick closed in ${lobby.name}`, target.user.displayAvatarURL())
					.setTitle(`:x: Not enough votes (${target.displayName})`)
					.setDescription(`:clock10: Votekick expired after 2 minutes\n:information_source: **Reason:** ${reason} **| :clipboard: Results:** ${yesList.length} yes, ${noList.length} no (${pendingList.length} abstain)`)
					.setFooter(`Votekick initiated by ${host.user.tag} | ${host.user.id}`, host.user.displayAvatarURL())
					.setTimestamp();
				msg.channel.send(`||${members.map(m => m.toString()).join(' ')}||`, embed)
			}
			return;
		} 

		function applyVotekick(target, lobby) {
			target.send(`:boot: **You have been votekicked from ${lobby.name} for ${reason}!**`).catch(console.error); // \n:clock10: You will be unable to join ${lobby.name} for **30 minutes.** Don't attempt to bother this lobby further - you will be punished.
			if (target.voice.channel == lobby) { // Check if target is still in lobby
				target.voice.kick(`Votekick`).catch(console.error);
            }
			//lobby.createOverwrite(target, {
			//	CONNECT: false
			//}).catch(console.error);
			//setTimeout(expireCooldown(target, lobby), 1000 * 60 * 1); // i give up
			function expireCooldown(target, lobby) {
				console.log(target.id);
				lobby.permissionOverwrites.get(target.id).delete()
					.then(console.log(`>> Expired cooldown for ${target.user.tag} in ${lobby.name}`))
				target.send(`:clock10: Your cooldown in **${lobby.name}** has expired.`).catch(console.error);
			}
		}


	}
};