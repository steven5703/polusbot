const { CommandoClient } = require('discord.js-commando');
const path = require('path');
const config = require('./config/main.json');
const private = require('./config/private.json');
const np = require('node-persist')

const client = new CommandoClient({
	commandPrefix: '.',
	owner: ['401937592142135297', '504303850874732554'],
	disableEveryone: true
});

client.registry // Register Commands
	.registerDefaultTypes()
	.registerGroups([
		['lobbymgr', 'Party Manager'],
		['general', 'General Commands']
	])
	.registerDefaultGroups()
	.registerDefaultCommands({
		ping: false,
		help: false,
		unknownCommand: false,
	})
	.registerCommandsIn(path.join(__dirname, 'commands'));

//Login via token in private.json. Add private.json to gitignore!!!!
client.login(private.token);

//Startup Operations
client.once('ready', () => {
	var logMsg = `Logged in: ${client.user.tag} (${client.user.id})`; // log message format, will probably use this format throughout the bot
	console.log(logMsg);
	client.user.setActivity('.invite | .help', { type: 'WATCHING' });
	client.channels.cache.get(config.logChannel).send(`:satellite: **Ready!** \`\`\`${logMsg}\`\`\``);
	initNp();
	async function initNp() {
		await np.init();
    }
});

client.setInterval(statusChange, 5000);
var statusState = 0;
async function statusChange() {
	if (statusState == 0) {
		statusState++;
		return client.user.setActivity('.skeld | .help', { type: 'WATCHING' });
	} else if (statusState == 1) {
		statusState++;
		return client.user.setActivity('.mira | .help', { type: 'WATCHING' });
	} else if (statusState == 2) {
		statusState++;
		return client.user.setActivity('.polus | .help', { type: 'WATCHING' });
	} else if (statusState == 3) {
		statusState = 0;
		return client.user.setActivity('.invite | .help', { type: 'WATCHING' });
	}
}

client.on('message', (msg) => {
	if (msg.content.includes(private.token)) { // Token leak check
		console.log(`!!!!!!!!!! TOKEN LEAK IN ${msg.channel.name} BY ${msg.author.tag}.\n!!!!!!!!!! TOKEN LEAK IN ${msg.channel.name} BY ${msg.author.tag}.\n!!!!!!!!!! TOKEN LEAK IN ${msg.channel.name} BY ${msg.author.tag}.\n!!!!!!!!!! TOKEN LEAK IN ${msg.channel.name} BY ${msg.author.tag}.\n!!!!!!!!!! TOKEN LEAK IN ${msg.channel.name} BY ${msg.author.tag}.\n!!!!!!!!!! TOKEN LEAK IN ${msg.channel.name} BY ${msg.author.tag}.\n `)
		msg.delete();
	}
	// Invite channel restrictor
	if (msg.author.bot) { // dont delete msg if bot
		return;
    }
	if (msg.channel.id !== config.inviteChannel || !config.restrictInviteChannel) { // check if invite chan restriction is enabled, check if message is in invite channel
		return;
	} else {
		if (msg.content.startsWith(".")) { // lazy check if command
			return;
		}
		let perms = msg.member.permissionsIn(msg.channel).toArray(); // don't delete message if sent by mod, server admin, me, etc.
		if (perms.includes("MANAGE_MESSAGES")) {
			return console.log(`>> ${msg.author.tag} is exempt from invite restrict in ${msg.channel.name}`);
		} else {
			msg.delete(); // if message is not invite or sent by admin/bot, delete it and DM user.
			return msg.author.send(`:x: Only \`.invite\` may be sent to LFG. To send an invite, use \`.invite <map> [crewlink server]\` in the LFG channel.\n:information_source: A map is required, the Crewlink server is optional. You may use \`skeld\`, \`mira\`, or \`polus\` for map. If you do not specify a Crewlink server, the recommended one, which is currently \`${config.voiceServer}\`, will be used.\nFor more information, run \`.invite help\` in the LFG channel.`).catch(console.error);
		}
    }
});
